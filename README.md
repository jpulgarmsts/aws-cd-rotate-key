# aws-cd-rotate-key

Project to automate the rotating of AWS keys in Gitlab.com and AWS SSM for CD and app users.

# Workflow

1. Make sure the AWS User you are about to change only has 1 active Access Key. Delete any inactive keys before running script.
2. Run the script for the user.
3. Do a Deployment.
4. Manually Inactivate/delete the old keys ONLY after AWS shows the new keys are now being used.

# Usage

You will need to be authenticated with aws-mfa and create a Gitlab Personal Token named "temp" (see GITLAB_ACCESS_TOKEN section below).

To test the changes that will take place:
```shell
aws-mfa
GITLAB_ACCESS_TOKEN=123456 YAML_CONFIG=test DRY_RUN=true docker compose up
```

To actually do the changes:
```shell
aws-mfa
GITLAB_ACCESS_TOKEN=123456 YAML_CONFIG=test DRY_RUN=false docker compose up
```

## Environment Variables
### GITLAB_ACCESS_TOKEN
You get this value from:
https://gitlab.com/-/profile/personal_access_tokens

- Name: temp
- Expires at: 2 days from now (same day doesn't work, next day doesn't allow post 9pm deployments)
- Scope: api

### YAML_CONFIG
Corresponds to the name (without .yml extension) of the files within configs/ folder.

### DRY_RUN
- true = It will only show you what it will do. To test your config.
- false = It will actually do what's specified in the config.

## Sample Config
```yml
users:
  - CDEnterprisemessagingDevelopment:
      - gitlab_project_id: 12345
        gitlab_variable_type: project
        gitlab_access_key_name: AWS_ACCESS_KEY_ID
        gitlab_secret_access_key_name: AWS_SECRET_ACCESS_KEY
        gitlab_environment: all
        ssm_access_key_name: myproject.development.aws-access-key-id
        ssm_secret_access_key_name: myproject.development.aws-secret-access-key
        aws_zone: us-east-1
```
What's happening above?
1. For AWS user CDEnterprisemessagingDevelopment, we will CREATE a new AWS Access + Private Key.
2. We will then UPDATE the Project-level variables of Project ID 12345 named AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY with those new values for ALL Gitlab-defined environments.
3. We will then UPDATE the AWS SSM key named myproject.development.aws-access-key-id and myproject.development.aws-secret-access-key with those new values.

## Sample Config (more complex)
```yml
users:
  - CDEnterprisemessagingDevelopment:
      - gitlab_project_id: 12345
        gitlab_variable_type: project
        gitlab_access_key_name: AWS_ACCESS_KEY_ID
        gitlab_secret_access_key_name: AWS_SECRET_ACCESS_KEY
        gitlab_environment: development
      - gitlab_project_id: 67890
        gitlab_variable_type: project
        gitlab_access_key_name: AWS_ACCESS_KEY_ID
        gitlab_secret_access_key_name: AWS_SECRET_ACCESS_KEY
        gitlab_environment: development
  - CDEnterprisemessagingProduction:
      - gitlab_project_id: 11223
        gitlab_variable_type: group
        gitlab_access_key_name: PROD_AWS_ACCESS_KEY_ID
        gitlab_secret_access_key_name: PROD_AWS_SECRET_ACCESS_KEY
        gitlab_environment: production
  - CDEnterprisemessagingStaging:
      - ssm_access_key_name: myproject.development.aws-access-key-id
        ssm_secret_access_key_name: myproject.development.aws-secret-access-key
        aws_zone: us-east-1
```

What's happening above?
1. For AWS user CDEnterprisemessagingDevelopment, we will CREATE a new AWS Access + Private Key.
2. We will then UPDATE the Project-level variables of Project ID 12345 named AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY with those new values for development Gitlab-defined environments.
3. We will then UPDATE the Project-level variables of Project ID 67890 named AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY with those new values for development Gitlab-defined environments.
4. For AWS user CDEnterprisemessagingProduction, we will CREATE a new AWS Access + Private Key.
5. We will then UPDATE the GROUP-level variables of Project ID 11223 named PROD_AWS_ACCESS_KEY_ID and PROD_AWS_SECRET_ACCESS_KEY with those new values for production Gitlab-defined environments.
6. For AWS user CDEnterprisemessagingStaging, we will then UPDATE the AWS SSM key named myproject.development.aws-access-key-id and myproject.development.aws-secret-access-key with those new values.
