package main

import (
	"context"
	"errors"
	"log"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/iam"
	"github.com/aws/aws-sdk-go-v2/service/ssm"
	"github.com/aws/aws-sdk-go-v2/service/ssm/types"
)

func validAWSZone(zone string) bool {
	switch zone {
	case
		"af-south-1",
		"ap-east-1",
		"ap-northeast-1",
		"ap-northeast-2",
		"ap-northeast-3",
		"ap-south-1",
		"ap-southeast-1",
		"ap-southeast-2",
		"ca-central-1",
		"eu-central-1",
		"eu-north-1",
		"eu-south-1",
		"eu-west-1",
		"eu-west-2",
		"eu-west-3",
		"me-south-1",
		"sa-east-1",
		"us-east-1",
		"us-east-2",
		"us-west-1",
		"us-west-2":
		return true
	}
	return false
}

func createIAMAWSClient(debug bool) (*iam.Client, error) {
	var lm aws.ClientLogMode
	if debug {
		lm |= aws.LogRequestWithBody
		lm |= aws.LogResponseWithBody
	}

	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithRegion("us-east-1"), config.WithClientLogMode(lm))
	if err != nil {
		return nil, err
	}
	return iam.NewFromConfig(cfg), nil
}

func createSSMAWSClient(debug bool, zone string) (*ssm.Client, error) {
	var lm aws.ClientLogMode
	if debug {
		lm |= aws.LogRequestWithBody
		lm |= aws.LogResponseWithBody
	}

	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithRegion(zone), config.WithClientLogMode(lm))
	if err != nil {
		return nil, err
	}

	return ssm.NewFromConfig(cfg), nil
}

// IAM
// Examples:
// https://aws.github.io/aws-sdk-go-v2/docs/code-examples/iam/listaccesskeys/
// https://aws.github.io/aws-sdk-go-v2/docs/code-examples/iam/createaccesskey/

// IAMListAccessKeysAPI defines the interface for the ListAccessKeys function.
type IAMListAccessKeysAPI interface {
	ListAccessKeys(ctx context.Context,
		params *iam.ListAccessKeysInput,
		optFns ...func(*iam.Options)) (*iam.ListAccessKeysOutput, error)
}

func canCreateMoreKeys(c context.Context, api IAMListAccessKeysAPI, input *iam.ListAccessKeysInput) error {
	resultListKeys, err := api.ListAccessKeys(c, input)
	if err != nil {
		return errors.New("Unable to get a list of keys for " + *input.UserName + ".")
	}

	if len(resultListKeys.AccessKeyMetadata) > 1 {
		return errors.New(*input.UserName + " already has 2 keys. Delete one before running script.")
	} else {
		return nil
	}
}

// IMACreateAccessKeyAPI defines the interface for the CreateAccessKey function.
type IAMCreateAccessKeyAPI interface {
	CreateAccessKey(ctx context.Context,
		params *iam.CreateAccessKeyInput,
		optFns ...func(*iam.Options)) (*iam.CreateAccessKeyOutput, error)
}

func MakeAccessKey(dryrun bool, c context.Context, api IAMCreateAccessKeyAPI, username string) (string, string, error) {

	if !dryrun {
		inputCreateKey := &iam.CreateAccessKeyInput{
			UserName: aws.String(username),
		}
		result, err := api.CreateAccessKey(c, inputCreateKey)
		if err != nil {
			return "", "", errors.New("Unable to create keys for " + username + ".")
		}
		log.Println("For AWS IAM User: " + username)
		log.Println("  Created new access key with ID: " + *result.AccessKey.AccessKeyId + " and secret key: " + *result.AccessKey.SecretAccessKey)
		return *result.AccessKey.AccessKeyId, *result.AccessKey.SecretAccessKey, nil
	} else {
		log.Println("[DRY-RUN] For AWS IAM User: " + username)
		log.Println("[DRY-RUN]   Created new access key with ID: XXXXXXXX and secret key: XXXXXXXX (dummy values)")
		return "XXXXXXXX", "XXXXXXXX", nil
	}
}

// SSM
// Example: https://aws.github.io/aws-sdk-go-v2/docs/code-examples/ssm/putparameter/

// SSMPutParameterAPI defines the interface for the PutParameter function.
type SSMPutParameterAPI interface {
	PutParameter(ctx context.Context,
		params *ssm.PutParameterInput,
		optFns ...func(*ssm.Options)) (*ssm.PutParameterOutput, error)
}

func AddStringParameter(dryrun bool, c context.Context, api SSMPutParameterAPI, key string, value string) {
	if key == "" {
		return
	}
	if !dryrun {

		overwrite := true
		ssmAccessInput := &ssm.PutParameterInput{
			Name:      aws.String(key),
			Value:     aws.String(value),
			Type:      types.ParameterTypeSecureString,
			Overwrite: &overwrite,
		}
		_, err := api.PutParameter(c, ssmAccessInput)

		if err != nil {
			log.Println(err.Error())
			return
		}

		log.Println("    Updated SSM variable named " + key + " to: " + value)
		time.Sleep(1 * time.Second) // so we don't flood AWS
	} else {
		log.Println("[DRY-RUN]     Update SSM variable named " + key + " to: " + value)
	}
}
