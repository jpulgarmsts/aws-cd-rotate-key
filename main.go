package main

import (
	"context"
	"io/ioutil"
	"log"
	"os"
	"strconv"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/iam"
	"github.com/aws/aws-sdk-go-v2/service/ssm"
	"gopkg.in/yaml.v3"
)

// YAML Structure
type Users struct {
	User []User `yaml:"users"`
}

type User map[string][]Repo

type Repo struct {
	GitlabProjectID           string `yaml:"gitlab_project_id,omitempty"`
	GitlabVariableType        string `yaml:"gitlab_variable_type,omitempty"`
	GitlabAccessKeyName       string `yaml:"gitlab_access_key_name,omitempty"`
	GitlabSecretAccessKeyName string `yaml:"gitlab_secret_access_key_name,omitempty"`
	GitlabEnvironment         string `yaml:"gitlab_environment,omitempty"`
	SSMAccessKeyName          string `yaml:"ssm_access_key_name,omitempty"`
	SSMSecretAccessKeyName    string `yaml:"ssm_secret_access_key_name,omitempty"`
	AWSZone                   string `yaml:"aws_zone,omitempty"`
}

// Usage: GITLAB_ACCESS_TOKEN=123456 YAML_CONFIG=test DRY_RUN=true docker compose up
func main() {

	// Check if we are doing a dry run
	dryRun, err := strconv.ParseBool(os.Getenv("DRY_RUN"))
	if err != nil {
		log.Println("Invalid value for DRY_RUN environment variable. Use true or false.")
		return
	}

	// Read YAML Config
	out := Users{}
	if err = readConfig(os.Getenv("YAML_CONFIG"), &out); err != nil {
		log.Println(err)
		return
	}

	// Set up AWS Clients. Load the Shared AWS Configuration (/root/config)
	ssmClients := make(map[string]*ssm.Client)
	var iamClient *iam.Client
	debugAWSClient := false

	// IAM Client
	iamClient, err = createIAMAWSClient(debugAWSClient)
	if err != nil {
		log.Println(err)
		return
	}

	// SSM Clients
	for _, users := range out.User {
		for _, repos := range users {
			for _, repo := range repos {
				if ssmClients[repo.AWSZone] == nil {
					if !validAWSZone(repo.AWSZone) {
						log.Println(repo.AWSZone + " is not a valid AWS Zone!")
						return
					}
					ssmClients[repo.AWSZone], err = createSSMAWSClient(debugAWSClient, repo.AWSZone)
					if err != nil {
						log.Println(err)
						return
					}
				}
			}
		}
	}

	// Make sure all user accounts in YAML config have less than 2 keys (there is a 2 key creation limit)
	for _, users := range out.User {
		for username := range users {
			if err = canCreateMoreKeys(context.TODO(), iamClient, &iam.ListAccessKeysInput{UserName: aws.String(username)}); err != nil {
				log.Println(err)
				return
			}
		}
	}

	// For each user, create a new key and update gitlab + SSM
	for _, users := range out.User {
		for username, repos := range users {

			// Generate new AWS key
			newKey, newSecretKey, err := MakeAccessKey(dryRun, context.TODO(), iamClient, username)

			if err != nil {
				log.Println(err)
				return
			}
			for _, repo := range repos {

				// Update Gitlab values
				getGitlabKey(os.Getenv("GITLAB_ACCESS_TOKEN"), repo.GitlabProjectID, repo.GitlabVariableType, repo.GitlabAccessKeyName, repo.GitlabEnvironment)
				updateGitlabKey(dryRun, os.Getenv("GITLAB_ACCESS_TOKEN"), repo.GitlabProjectID, repo.GitlabVariableType, repo.GitlabAccessKeyName, newKey, repo.GitlabEnvironment)
				getGitlabKey(os.Getenv("GITLAB_ACCESS_TOKEN"), repo.GitlabProjectID, repo.GitlabVariableType, repo.GitlabSecretAccessKeyName, repo.GitlabEnvironment)
				updateGitlabKey(dryRun, os.Getenv("GITLAB_ACCESS_TOKEN"), repo.GitlabProjectID, repo.GitlabVariableType, repo.GitlabSecretAccessKeyName, newSecretKey, repo.GitlabEnvironment)

				// SSM Updates
				AddStringParameter(dryRun, context.TODO(), ssmClients[repo.AWSZone], repo.SSMAccessKeyName, newKey)
				AddStringParameter(dryRun, context.TODO(), ssmClients[repo.AWSZone], repo.SSMSecretAccessKeyName, newSecretKey)

			}

		}
	}

}

func readConfig(filename string, configObject *Users) error {

	configFilename := "configs/" + filename + ".yml"
	buf, err := ioutil.ReadFile(configFilename)
	if err != nil {
		return err
	}

	if err := yaml.Unmarshal([]byte(buf), &configObject); err != nil {
		return err
	} else {
		log.Println("Using config: " + configFilename)
		return nil
	}

}
