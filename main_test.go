package main

import (
	"bytes"
	"log"
	"os"
	"testing"
)

func TestFileNotFoundReadConfig(t *testing.T) {
	var buf bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&buf)
	defer func() {
		log.SetOutput(os.Stderr)
	}()
	out := Users{}
	err := readConfig("fileNotFound", &out)

	actual := err.Error()
	expected := "open configs/fileNotFound.yml: no such file or directory"

	if actual != expected {
		t.Fail()
	}
}

func TestValidReadConfig(t *testing.T) {
	var buf bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&buf)
	defer func() {
		log.SetOutput(os.Stderr)
	}()
	out := Users{}
	err := readConfig("tests/valid-simple", &out)

	actual := buf.String()
	expected := "Using config: configs/tests/valid-simple.yml\n"

	if err == nil && actual != expected {
		t.Fail()
	}
}

func TestBadReadConfig(t *testing.T) {
	var buf bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&buf)
	defer func() {
		log.SetOutput(os.Stderr)
	}()
	out := Users{}
	err := readConfig("tests/invalid", &out)

	if err == nil {
		t.Fail()
	}
}
