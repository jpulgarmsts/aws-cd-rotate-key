FROM golang:1.21.6-bookworm
# These commands now need to be run locally on your computer instead of Dockerfile:
# go get gopkg.in/yaml.v3@latest
# go get github.com/aws/aws-sdk-go-v2@latest
# go get github.com/aws/aws-sdk-go-v2/config@latest
# go get github.com/aws/aws-sdk-go-v2/service/iam@latest
# go get github.com/aws/aws-sdk-go-v2/service/ssm@latest
# go get github.com/jarcoal/httpmock@latest
WORKDIR /usr/app

# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .