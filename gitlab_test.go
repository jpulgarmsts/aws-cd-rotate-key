package main

import (
	"bytes"
	"log"
	"os"
	"testing"

	"github.com/jarcoal/httpmock"
)

func TestEmptyKeyName(t *testing.T) {
	var buf bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&buf)
	defer func() {
		log.SetOutput(os.Stderr)
	}()
	updateGitlabKey(true, "gitlab1234", "1234", "project", "", "dummy", "development")
	actual := buf.String()
	expected := ""

	if actual != expected {
		t.Fail()
		t.Logf("Expected blank key name not to print anything.")
	}
}

func TestFilledKeyName(t *testing.T) {
	var buf bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&buf)
	defer func() {
		log.SetOutput(os.Stderr)
	}()
	updateGitlabKey(true, "gitlab1234", "1234", "project", "KEY_NAME", "dummy", "development")
	actual := buf.String()
	expected := "[DRY-RUN]     For Project ID #1234 Update project-level variable named KEY_NAME to: dummy (Gitlab Environment: development)\n"

	if actual != expected {
		t.Fail()
	}
}

func TestEmptyGitlabKey(t *testing.T) {
	var buf bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&buf)
	defer func() {
		log.SetOutput(os.Stderr)
	}()

	updateGitlabKey(false, "gitlab1234", "1234", "project", "", "dummy", "development")

	actual := buf.String()
	expected := ""

	if actual != expected {
		t.Fail()
	}
}

func TestValidGitlabPutRequest(t *testing.T) {
	var buf bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&buf)
	defer func() {
		log.SetOutput(os.Stderr)
		httpmock.DeactivateAndReset()
	}()
	httpmock.Activate()

	httpmock.RegisterResponder("PUT", "https://gitlab.com/api/v4/projects/1234/variables/KEY_NAME?filter[environment_scope]=*",
		httpmock.NewStringResponder(200, ""))

	updateGitlabKey(false, "gitlab1234", "1234", "project", "KEY_NAME", "dummy", "all")

	actual := buf.String()
	expected := "GITLAB AFTER CHANGE:\n    Updated Project ID #1234 project-level variable named KEY_NAME to: dummy (Gitlab Environment: *)\n"

	if actual != expected {
		t.Fail()
	}
}

func TestBadGitlabPutRequest(t *testing.T) {
	var buf bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&buf)
	defer func() {
		log.SetOutput(os.Stderr)
		httpmock.DeactivateAndReset()
	}()
	httpmock.Activate()

	httpmock.RegisterResponder("PUT", "https://gitlab.com/api/v4/XYZs/1234/variables/KEY_NAME?filter[environment_scope]=*",
		httpmock.NewStringResponder(500, "Internal Server Error"))

	updateGitlabKey(false, "gitlab1234", "1234", "XYZ", "KEY_NAME", "dummy", "all")

	actual := buf.String()
	expected := "Internal Server Error\n"

	if actual != expected {
		t.Fail()
	}
}
