package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

// For Gitlab.com API
type UpdateProjectVariableOptions struct {
	Value            string `json:"value,omitempty"`
	VariableType     string `json:"variable_type,omitempty"`
	Protected        bool   `json:"protected,omitempty"`
	Masked           bool   `json:"masked,omitempty"`
	EnvironmentScope string `json:"environment_scope,omitempty"`
}

func PrettyJSONString(str string) (string, error) {
	var prettyJSON bytes.Buffer
	if err := json.Indent(&prettyJSON, []byte(str), "", "    "); err != nil {
		return "", err
	}
	return prettyJSON.String(), nil
}

func getGitlabKey(gitlabkey string, id string, variableType string, name string, environment string) {

	if strings.EqualFold(environment, "all") {
		environment = "*"
	}

	req, err := http.NewRequest("GET", "https://gitlab.com/api/v4/"+variableType+"s/"+id+"/variables/"+name+"?filter[environment_scope]="+environment, nil)
	if err != nil {
		log.Println(err)
	}
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Private-Token", gitlabkey)

	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
		}
		log.Println("GITLAB BEFORE CHANGE:")
		res, err := PrettyJSONString(string(bodyBytes))
		if err != nil {
			log.Fatal(err)
		}
		log.Println(res)
		time.Sleep(1 * time.Second) // so we don't flood Gitlab
	} else {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
		}
		log.Println((string(bodyBytes)))
	}
}

func updateGitlabKey(dryrun bool, gitlabkey string, id string, variableType string, name string, value string, environment string) {
	if name == "" {
		return
	}

	if !dryrun {
		pb := &UpdateProjectVariableOptions{Value: value}
		jsonStr, err := json.Marshal(pb)
		if err != nil {
			log.Printf("Could not marshal JSON: %s", err)
		}

		if strings.EqualFold(environment, "all") {
			environment = "*"
		}

		req, err := http.NewRequest("PUT", "https://gitlab.com/api/v4/"+variableType+"s/"+id+"/variables/"+name+"?filter[environment_scope]="+environment, bytes.NewBuffer(jsonStr))
		if err != nil {
			log.Println(err)
		}
		req.Header.Set("Content-Type", "application/json; charset=utf-8")
		req.Header.Set("Private-Token", gitlabkey)

		resp, err := http.DefaultClient.Do(req)

		if err != nil {
			log.Println(err)
		}
		defer resp.Body.Close()

		if resp.StatusCode == http.StatusOK {
			bodyBytes, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Println(err)
			}
			log.Println("GITLAB AFTER CHANGE:")
			if string(bodyBytes) != "" {
				res, err := PrettyJSONString(string(bodyBytes))
				if err != nil {
					log.Fatal(err)
				}
				log.Println(res)
			}
			log.Println("    Updated Project ID #" + id + " " + variableType + "-level variable named " + name + " to: " + value + " (Gitlab Environment: " + environment + ")")
			time.Sleep(1 * time.Second) // so we don't flood Gitlab
		} else {
			bodyBytes, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Println(err)
			}
			log.Println((string(bodyBytes)))
		}
	} else {
		log.Println("[DRY-RUN]     For Project ID #" + id + " Update " + variableType + "-level variable named " + name + " to: " + value + " (Gitlab Environment: " + environment + ")")
	}
}
