package main

import (
	"bytes"
	"context"
	"errors"
	"log"
	"os"
	"reflect"
	"strconv"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/iam"
	"github.com/aws/aws-sdk-go-v2/service/iam/types"
	"github.com/aws/aws-sdk-go-v2/service/ssm"
)

// *************************
// CREATE AWS CLIENTS TESTS
// *************************
func TestCreateIAMClient(t *testing.T) {
	testIAMClient, error := createIAMAWSClient(true)
	if reflect.TypeOf(testIAMClient).String() != "*iam.Client" {
		t.Errorf("expected to get *iam.Client back")
	}
	if error != nil {
		t.Errorf("expect no error, got %v", error.Error())
	}
}

func TestCreateSSMClient(t *testing.T) {
	testSSMClient, error := createSSMAWSClient(true, "us-east-1")
	if reflect.TypeOf(testSSMClient).String() != "*ssm.Client" {
		t.Errorf("expected to get *ssm.Client back")
	}
	if error != nil {
		t.Errorf("expect no error, got %v", error.Error())
	}
}
func TestValidAWSZone(t *testing.T) {
	if !validAWSZone("us-east-1") {
		t.Errorf("expected us-east-1 to be a valid zone")
	}
}

func TestInvalidAWSZone(t *testing.T) {
	if validAWSZone("bad-zone-1") {
		t.Errorf("expected bad-zone-1 to be an invalid zone")
	}
}

// ****************
// LIST KEYS TESTS
// ****************
type mockIAMListAccessKeysAPI func(ctx context.Context, params *iam.ListAccessKeysInput, optFns ...func(*iam.Options)) (*iam.ListAccessKeysOutput, error)

func (m mockIAMListAccessKeysAPI) ListAccessKeys(ctx context.Context, params *iam.ListAccessKeysInput, optFns ...func(*iam.Options)) (*iam.ListAccessKeysOutput, error) {
	return m(ctx, params, optFns...)
}

func TestMaxNumKeysReached(t *testing.T) {
	cases := []struct {
		client func(t *testing.T) IAMListAccessKeysAPI
		expect error
	}{
		{
			client: func(t *testing.T) IAMListAccessKeysAPI {
				return mockIAMListAccessKeysAPI(func(ctx context.Context, params *iam.ListAccessKeysInput, optFns ...func(*iam.Options)) (*iam.ListAccessKeysOutput, error) {
					t.Helper()
					metaData := []types.AccessKeyMetadata{
						{
							AccessKeyId: aws.String("SAMPLE_KEY_ONE"),
							Status:      types.StatusTypeActive,
						},
						{
							AccessKeyId: aws.String("SAMPLE_KEY_TWO"),
							Status:      types.StatusTypeActive,
						},
					}
					return &iam.ListAccessKeysOutput{
						AccessKeyMetadata: metaData,
					}, nil // this nil is the error
				})
			},
			expect: errors.New("username already has 2 keys. Delete one before running script."),
		},
	}

	for i, tt := range cases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			ctx := context.TODO()
			content := canCreateMoreKeys(ctx, tt.client(t), &iam.ListAccessKeysInput{UserName: aws.String("username")})
			if expected, actual := tt.expect, content; expected.Error() != actual.Error() {
				t.Errorf("expect %v, got %v", expected, actual)
			}
		})
	}
}

func TestMaxNumKeysNotReached(t *testing.T) {
	cases := []struct {
		client func(t *testing.T) IAMListAccessKeysAPI
		expect error
	}{
		{
			client: func(t *testing.T) IAMListAccessKeysAPI {
				return mockIAMListAccessKeysAPI(func(ctx context.Context, params *iam.ListAccessKeysInput, optFns ...func(*iam.Options)) (*iam.ListAccessKeysOutput, error) {
					t.Helper()
					metaData := []types.AccessKeyMetadata{
						{
							AccessKeyId: aws.String("SAMPLE_KEY_ONE"),
							Status:      types.StatusTypeActive,
						},
					}
					return &iam.ListAccessKeysOutput{
						AccessKeyMetadata: metaData,
					}, nil // this nil is the error
				})
			},
			expect: nil,
		},
	}

	for i, tt := range cases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			ctx := context.TODO()
			content := canCreateMoreKeys(ctx, tt.client(t), &iam.ListAccessKeysInput{UserName: aws.String("username")})
			if expected, actual := tt.expect, content; expected != actual {
				t.Errorf("expect %v, got %v", expected, actual)
			}
		})
	}
}

func TestUnableToListKeys(t *testing.T) {
	cases := []struct {
		client func(t *testing.T) IAMListAccessKeysAPI
		expect error
	}{
		{
			client: func(t *testing.T) IAMListAccessKeysAPI {
				return mockIAMListAccessKeysAPI(func(ctx context.Context, params *iam.ListAccessKeysInput, optFns ...func(*iam.Options)) (*iam.ListAccessKeysOutput, error) {
					t.Helper()

					return nil, errors.New("api.ListAccessKeys error") // this nil is the error
				})
			},
			expect: errors.New("Unable to get a list of keys for username."),
		},
	}

	for i, tt := range cases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			ctx := context.TODO()
			content := canCreateMoreKeys(ctx, tt.client(t), &iam.ListAccessKeysInput{UserName: aws.String("username")})
			if expected, actual := tt.expect, content; expected.Error() != actual.Error() {
				t.Errorf("expect %v, got %v", expected, actual)
			}
		})
	}
}

// *****************
// CREATE KEYS TESTS
// *****************
type mockIAMCreateAccessKeyAPI func(ctx context.Context, params *iam.CreateAccessKeyInput, optFns ...func(*iam.Options)) (*iam.CreateAccessKeyOutput, error)

func (m mockIAMCreateAccessKeyAPI) CreateAccessKey(ctx context.Context, params *iam.CreateAccessKeyInput, optFns ...func(*iam.Options)) (*iam.CreateAccessKeyOutput, error) {
	return m(ctx, params, optFns...)
}
func TestUnableToCreateKeys(t *testing.T) {
	cases := []struct {
		client func(t *testing.T) IAMCreateAccessKeyAPI
		expect error
	}{
		{
			client: func(t *testing.T) IAMCreateAccessKeyAPI {
				return mockIAMCreateAccessKeyAPI(func(ctx context.Context, params *iam.CreateAccessKeyInput, optFns ...func(*iam.Options)) (*iam.CreateAccessKeyOutput, error) {
					t.Helper()

					return nil, errors.New("api.CreateAccessKeys error")
				})
			},
			expect: errors.New("Unable to create keys for username."),
		},
	}

	for i, tt := range cases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			ctx := context.TODO()
			_, _, actual := MakeAccessKey(false, ctx, tt.client(t), "username")
			if expected, actual := tt.expect, actual; expected.Error() != actual.Error() {
				t.Errorf("expect %v, got %v", expected, actual)
			}
		})
	}
}

func TestAbleToCreateKeys(t *testing.T) {
	cases := []struct {
		client func(t *testing.T) IAMCreateAccessKeyAPI
	}{
		{
			client: func(t *testing.T) IAMCreateAccessKeyAPI {
				return mockIAMCreateAccessKeyAPI(func(ctx context.Context, params *iam.CreateAccessKeyInput, optFns ...func(*iam.Options)) (*iam.CreateAccessKeyOutput, error) {
					t.Helper()
					return &iam.CreateAccessKeyOutput{
						AccessKey: &types.AccessKey{AccessKeyId: aws.String("SAMPLE_ACCESS_KEY"),
							SecretAccessKey: aws.String("SAMPLE_SECRET_KEY"),
							Status:          types.StatusTypeActive,
							UserName:        aws.String("username")},
					}, nil
				})
			},
		},
	}

	for i, tt := range cases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			ctx := context.TODO()
			accessKey, secretKey, error := MakeAccessKey(false, ctx, tt.client(t), "username")
			if accessKey != "SAMPLE_ACCESS_KEY" {
				t.Error("expect SAMPLE_ACCESS_KEY, got " + accessKey)
			}
			if secretKey != "SAMPLE_SECRET_KEY" {
				t.Error("expect SAMPLE_SECRET_KEY, got " + secretKey)
			}
			if error != nil {
				t.Errorf("expect no error, got %v", error.Error())
			}
		})
	}
}

func TestDryRunAbleToCreateKeys(t *testing.T) {
	ctx := context.TODO()
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		log.Println(err)
		return
	}
	iamClient := iam.NewFromConfig(cfg)
	accessKey, secretKey, error := MakeAccessKey(true, ctx, iamClient, "username")
	if accessKey != "XXXXXXXX" {
		t.Error("expect XXXXXXXX, got " + accessKey)
	}
	if secretKey != "XXXXXXXX" {
		t.Error("expect XXXXXXXX, got " + secretKey)
	}
	if error != nil {
		t.Errorf("expect no error, got %v", error.Error())
	}
}

// *****************
// UPDATE SSM TESTS
// *****************

type mockSSMPutParameterAPI func(ctx context.Context, params *ssm.PutParameterInput, optFns ...func(*ssm.Options)) (*ssm.PutParameterOutput, error)

func (m mockSSMPutParameterAPI) PutParameter(ctx context.Context, params *ssm.PutParameterInput, optFns ...func(*ssm.Options)) (*ssm.PutParameterOutput, error) {
	return m(ctx, params, optFns...)
}

func TestUnableToUpdateSSM(t *testing.T) {

	cases := []struct {
		client func(t *testing.T) SSMPutParameterAPI
		expect string
	}{
		{
			client: func(t *testing.T) SSMPutParameterAPI {
				return mockSSMPutParameterAPI(func(ctx context.Context, params *ssm.PutParameterInput, optFns ...func(*ssm.Options)) (*ssm.PutParameterOutput, error) {
					t.Helper()
					return nil, errors.New("api.PutParameter error")
				})
			},
			expect: "api.PutParameter error\n",
		},
	}

	for i, tt := range cases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			var buf bytes.Buffer
			log.SetFlags(0)
			log.SetOutput(&buf)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			ctx := context.TODO()
			AddStringParameter(false, ctx, tt.client(t), "KEY_NAME", "KEY_VALUE")
			actual := buf.String()
			if actual != tt.expect {
				t.Fail()
				t.Logf("Expected to see error message.")
			}
		})
	}
}

func TestPassingBlankKeyToSSM(t *testing.T) {

	ctx := context.TODO()
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		log.Println(err)
		return
	}
	var buf bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&buf)
	defer func() {
		log.SetOutput(os.Stderr)
	}()
	ssmClient := ssm.NewFromConfig(cfg)
	AddStringParameter(false, ctx, ssmClient, "", "")
	actual := buf.String()
	if actual != "" {
		t.Fail()
		t.Logf("Expected to see no output when passing blank key.")
	}
}

func TestDryRunSSM(t *testing.T) {

	ctx := context.TODO()
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		log.Println(err)
		return
	}
	var buf bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&buf)
	defer func() {
		log.SetOutput(os.Stderr)
	}()
	ssmClient := ssm.NewFromConfig(cfg)
	AddStringParameter(true, ctx, ssmClient, "KEY_NAME", "KEY_VALUE")
	actual := buf.String()
	if actual != "[DRY-RUN]     Update SSM variable named KEY_NAME to: KEY_VALUE\n" {
		t.Fail()
		t.Logf("Expected to see dry run output.")
	}
}

func TestAbleToUpdateSSM(t *testing.T) {

	cases := []struct {
		client func(t *testing.T) SSMPutParameterAPI
		expect string
	}{
		{
			client: func(t *testing.T) SSMPutParameterAPI {
				return mockSSMPutParameterAPI(func(ctx context.Context, params *ssm.PutParameterInput, optFns ...func(*ssm.Options)) (*ssm.PutParameterOutput, error) {
					t.Helper()
					return &ssm.PutParameterOutput{Version: 2}, nil
				})
			},
			expect: "    Updated SSM variable named KEY_NAME to: KEY_VALUE\n",
		},
	}

	for i, tt := range cases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			var buf bytes.Buffer
			log.SetFlags(0)
			log.SetOutput(&buf)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			ctx := context.TODO()
			AddStringParameter(false, ctx, tt.client(t), "KEY_NAME", "KEY_VALUE")
			actual := buf.String()
			if actual != tt.expect {
				t.Fail()
				t.Logf("Expected to see error message.")
			}
		})
	}
}
